#pragma once

#include "Obj.hpp"
#include "Util.hpp"

#include "Game.hpp"
#include "World.hpp"
#include "Snd.hpp"

class Gold : public Obj
{
public:


	Gold(const Pos& p)
		: Obj(TYPE_GOLD, p)
	{
		m_uFlags |= FLAG_WANTSGRID;


		float yDir = -1;
		while (yDir < 0)
		{
			// get positive gamma-distrib value for y
			yDir = Util::rand(true);
		}

		float xDir = Util::rand(true);
		float zDir = Util::rand(true);

		float dirlen = sqrt(xDir * xDir + yDir * yDir + zDir * zDir);
		xDir /= dirlen;
		yDir /= dirlen;
		zDir /= dirlen;

		m_velX = xDir;
		m_velY = yDir;
		m_velZ = zDir;

	}

	inline void Update() override
	{
		constexpr float sz = .1f;

		m_scrnBounds = MakeScrnBounds(m_pos, 20 * sz, 20 * sz, 0, -.5f);
		m_coll = Collider::FromPos(m_pos, (20 * .25f) / 2.f, (20 * .25f) / 4.f);

		float frcX{}, frcY{}, frcZ{};

		Game& g = Game::Get();
		World& w = *g.GetWorld();

		if (m_lifetime > 0)
		{
			m_lifetime--;
		}
		else
		{
			Remove();
			return;
		}

		if (PickedUp())
		{

			frcY += m_pickupFlySpeed;

			if (m_pos.y > w.GetCam().Y())
			{
				// Gold per gold
				w.AddGold(GOLDCOST_COIN);

				g.GetSnd().Play(SND_COIN);

				Remove();
			}

		}
		else
		{

			// grv
			frcY -= .1f;

			// bounce
			if (m_pos.y < 0)
			{
				if (abs(m_velY) > .1f)
					frcY += abs(m_velY) + .1f;
				else
				{
					// settle on the floor
					frcY = 0;
					m_pos.y = 0;
				}
			}

			// No longer need mouse down to 
			// pickup coins
			if (/*g.MouseDown() && */!g.KeyDown(VK_F8))
			{
				const Pos& cp = w.GetCursorWorldPos();

				float xDiff = m_pos.x - cp.x;
				float zDiff = m_pos.z - cp.z;

				float l = sqrt(xDiff * xDiff + zDiff * zDiff);
				if (l < 30.f)
				{
					const float minBound = .25f;
					m_pickupFlySpeed = ((Util::rand() * (1 - minBound)) + minBound) * .25f;
				}

			}

			// Push out of stuff
			{
				Obj* apColls[16];
				uint32_t nColliders = GetCollisions(apColls);

				for (uint32_t u = 0; u < nColliders; ++u)
				{
					Obj* o = apColls[u];


					if (o->m_type == TYPE_CHARACTER ||
						o->m_type == TYPE_GOLD)
					{
						continue;
					}

					constexpr float pushOutStrength = .025f;

					float dirX = m_pos.x - o->GetPos().x;
					float dirZ = m_pos.z - o->GetPos().z;

					float dirLen = sqrt(dirX * dirX + dirZ * dirZ);

					if (dirLen > .001f)
					{
						frcX += dirX / dirLen * pushOutStrength;
						frcZ += dirZ / dirLen * pushOutStrength;
					}
					else
					{
						// literally perfectly inside each other
						o->ForcePos({ m_pos.x + Util::rand() * .02f - .01f, 0, m_pos.z + Util::rand() * .02f - .01f });
					}


				}
			}

			// drag
			m_velX *= .95f;
			m_velY *= .95f;
			m_velZ *= .95f;

		}

		m_velX += frcX;
		m_velY += frcY;
		m_velZ += frcZ;

		m_pos.x += m_velX;
		m_pos.y += m_velY;
		m_pos.z += m_velZ;

	}

	inline void Render(uint32_t* pScrn) override
	{



		RenderAsBillboard(pScrn, { 65, 125, 20, 20 }, m_scrnBounds);

	}

	inline Collider* GetCollider() override { return &m_coll; }

	inline bool PickedUp()
	{
		return m_pickupFlySpeed != FLT_MAX;
	}

private:



	float m_velX, m_velY, m_velZ;
	Collider m_coll{};

	// flt max means coin not picked up
	float m_pickupFlySpeed = FLT_MAX;

	// 30 seconds
	uint32_t m_lifetime = 60 * 30;

};
