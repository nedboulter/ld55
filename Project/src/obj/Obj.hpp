#pragma once

#include "obj/ObjMem.hpp"
#include "obj/Coll.hpp"

struct TexRegion;

class Obj
{
public:

	enum FlagBits : uint32_t
	{
		FLAG_WANTSGRID = 1 << 0,
		FLAG_WANTSREMOVAL = 1 << 1
	};

	enum Type
	{
		TYPE_TREE,
		TYPE_CHARACTER,
		TYPE_TOWER,
		TYPE_BLOODPARTICLE,
		TYPE_GOLD

	};

	static uint32_t s_nextUUID;

	const Type m_type;
	const uint32_t m_uuid;

	Obj(Type type,
		const Pos& startPos)
		: m_type(type)
		, m_pos(startPos)
		, m_scrnBounds({})
		, m_uuid(s_nextUUID++)
		{}

	void ComputeAABB(
		float nudgeX = 0.f,
		float nudgeY = 0.f,
		float nudgeZ = 0.f)
	{

	}

protected:
	virtual void Update() = 0;

public:

	void Tick();

	virtual void Render(uint32_t* pScrn) {}

	virtual void Destroy() {}

	inline const ScrnBounds& GetScrnBounds() const { return m_scrnBounds; }

	inline const Pos& GetPos() const { return m_pos; }

	inline void ForcePos(const Pos& newPos)
	{
		m_pos = newPos;
	}

	inline uint32_t GetFlags() const { return m_uFlags; }

	inline virtual Collider* GetCollider()
	{
		// null represents non-collideable
		return nullptr;
	}

	inline void Remove()
	{
		m_uFlags |= FLAG_WANTSREMOVAL;
	}

	

protected:

	Pos m_pos;
	ScrnBounds m_scrnBounds;

	uint32_t m_uFlags{};

	ScrnBounds MakeScrnBounds(
		const Pos& pos,
		float renderSizeWMeters,
		float renderSizeHMeters,
		float propScrOffsX,
		float propScrOffsY);

	void RenderAsBillboard(
		uint32_t* pScrn,
		const TexRegion& sheetRegion,
		const ScrnBounds& scrnBounds);

	uint32_t GetCollisions(
		Obj* (&outResult)[16],
		float* pGridComplexity = nullptr);

private:



};
