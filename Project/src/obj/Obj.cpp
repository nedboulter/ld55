#include "pch.hpp"
#include "Obj.hpp"

#include "Textures.hpp"
#include "Game.hpp"
#include "obj/World.hpp"

uint32_t Obj::s_nextUUID = 0;

void Obj::Tick()
{

	// Update game obj
	Update();





}

ScrnBounds Obj::MakeScrnBounds(
	const Pos& pos,
	float renderSizeWMeters,
	float renderSizeHMeters,
	float propScrOffsX,
	float propScrOffsY)
{

	Game& g = Game::Get();

	Pos p = {
		pos.x,// + renderPosOffset.x,
		pos.y,// + renderPosOffset.y,
		pos.z// + renderPosOffset.z
	};

	float scrX, scrY, z;
	g.GetWorld()->WorldToScreen(p, scrX, scrY, &z);

	// Scale up meters by screen size
	// and persp divide
	float W = (renderSizeWMeters * WIDTH / 2.f) / z;
	float H = (renderSizeHMeters * WIDTH / 2.f) / z;

	return
	{
		scrX + W*propScrOffsX, scrY + H*propScrOffsY,
		W / 2.f,
		H / 2.f,
		z
	};

}

void Obj::RenderAsBillboard(
	uint32_t* pScrn,
	const TexRegion& sheetRegion,
	const ScrnBounds& scrnBounds)
{

	Game::Get().GetTextures().StretchBlit(pScrn,
		sheetRegion,
		(int)(scrnBounds.x - scrnBounds.hw),
		(int)(scrnBounds.y - scrnBounds.hh),
		(int)(scrnBounds.hw * 2.f),
		(int)(scrnBounds.hh * 2.f));

	//Game& g = Game::Get();
	//
	//Pos p = {
	//	m_pos.x + renderPosOffset.x,
	//	m_pos.y + renderPosOffset.y,
	//	m_pos.z + renderPosOffset.z };
	//
	//float scrX, scrY, z;
	//g.GetWorld()->WorldToScreen(p, scrX, scrY, &z);
	//
	//// Scale up meters by screen size
	//// and persp divide
	//float W = (renderSizeWMeters * WIDTH / 2.f) / z;
	//float H = (renderSizeHMeters * HEIGHT / 2.f) / z;
	//
	//g.GetTextures().StretchBlit(pScrn, sheetRegion,
	//	(int)(scrX - W / 2.f), (int)(scrY - W / 2.f), (int)W, (int)H);

}

uint32_t Obj::GetCollisions(
	Obj* (&outResult)[16],
	float* pGridComplexity)
{

	Collider* pThisColl = GetCollider();
	if (!pThisColl)
	{
		return 0;
	}

	uint32_t count = 0;

	World& w = *Game::Get().GetWorld();

	int gxStart = (int)std::floor(pThisColl->minX / 10.f);
	int gzStart = (int)std::floor(pThisColl->minZ / 10.f);

	int gxEnd = (int)std::ceil(pThisColl->maxX / 10.f);
	int gzEnd = (int)std::ceil(pThisColl->maxZ / 10.f);

	float c = 0;

	for (int z = gzStart; z < gzEnd; ++z)
	{
		for (int x = gxStart; x < gxEnd; ++x)
		{
			GridEntry* pCell = w.GetGridEntry(x, z);

			if (pCell)
			{

				for (uint32_t i = 0; i < pCell->m_objCount; ++i)
				{
					Obj* o = pCell->m_objs[i];

					if (o == this || !o) continue;

					if (o->m_type != TYPE_TREE && o->m_type != TYPE_BLOODPARTICLE)
						c += 1.f;

					Collider* c = o->GetCollider();
					if (c && c->Intersects(*pThisColl))
					{
						if (count < 16)
						{
							outResult[count++] = o;
						}
						else
						{
							DBGLOG("Out of colls");
						}
					}

				}

			}
			else
				DBGLOG("Grid coll check OOB!!");

		}
	}

	if (pGridComplexity)
		*pGridComplexity = c;

	return count;
}
