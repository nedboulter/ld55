#pragma once

#include "obj/Obj.hpp"

enum CharType
{
	CHARTYPE_ZOMBIE,
	CHARTYPE_TROOP
};

class Character : public Obj
{
public:

	const CharType charType;

	Character(const Pos& p, CharType charType);

	void Update();

	void Render(uint32_t* pScrn);

#if 0
	uint32_t GetCollisions(
		Obj* (&outResult)[16]);
#endif

	inline Collider* GetCollider()
	{
		return &m_coll;
	}

	inline bool IsDead()
	{
		return m_health == 0;
	}

	inline void Damage(Obj* pDealer, uint32_t u)
	{
		if (m_health > 0)
		{
			m_health -= std::min((uint32_t)m_health, u);

			if (IsDead())
				JustDied(pDealer);
			else
				JustHurt(pDealer);

		}
	}

	inline uint32_t GetHealth()
	{
		return (uint32_t)m_health;
	}

private:

	// back to 1 game was too easy
	static constexpr float howMuchBetterAreTroops = .8f /*2 times!!! They have swords*/;

	static constexpr float likelihoodToDamagePerSecond = 4.f;
	static constexpr float likelihoodToHitPerTick = likelihoodToDamagePerSecond / 60;

	static constexpr int damageMin = 40;
	static constexpr int damageMax = 75;

	Collider m_coll{};

	// Could be a tower or a troop
	Obj* m_target{};
	uint32_t m_targetUUID = UINT_MAX;

	float m_velX{}, m_velZ{};
	float m_frcX{}, m_frcZ{};

	static constexpr uint16_t xAnimPeriod = 40;
	static constexpr uint16_t numFrames = 4;
	uint16_t m_framesYOffs{};
	uint16_t m_framesAnimTime{};

	uint8_t m_health{};

	uint8_t m_hurtCooldown{};

	void DoTroopLogic(
		float gridComplexity,
		uint32_t collCnt,
		Obj** ppColls);

	void DoZombieLogic(
		uint32_t collCnt,
		Obj** ppColls);

	void PushOutOfStuff(
		uint32_t count,
		Obj** ppColliding);

	// assume pOpposer chartype is whatever this isnt
	void DoDamageLogic(Character* pOpposer);

	void DoAnim();

	void JustDied(Obj* pKiller);

	void JustHurt(Obj* pHurter);

};
