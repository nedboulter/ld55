#pragma once

#include "obj/Obj.hpp"
#include "Textures.hpp"

#include "Game.hpp"
#include "Snd.hpp"

class Tower : public Obj
{
public:

	Tower(const Pos& p, bool mainTower)
		: Obj(TYPE_TOWER, p)
		, m_mainTower(mainTower)
	{
		m_uFlags |= FLAG_WANTSGRID;

		if (mainTower)
		{
			m_health = 500;
		}
		else
		{
			// this is not gonna be clear :D
			m_health = 100;
		}
	}

	void Update() override;

	inline void Render(uint32_t* pScrn) override
	{

		int x = IsDead() ? 96 : 0;

		RenderAsBillboard(pScrn,
			m_mainTower ? TexRegion{x, 128, 48, 128} : TexRegion{ x, 154, 48, 102 },
			m_scrnBounds);

	}

	inline Collider* GetCollider() override { return &m_coll; }

	inline uint32_t GetHealth()
	{
		return m_health;
	}

	inline bool IsDead() { return m_health == 0; }

	inline void Damage(Obj* pDealer, uint32_t u)
	{
		if (m_health > 0)
		{
			m_health -= std::min((uint32_t)m_health, u);
			
			if (IsDead())
			{
				JustDied();
			}
			else
			{
				if (m_scrnBounds.OnScreen())
					Game::Get().GetSnd().Play(SND_FOOTSTEP);
			}
		}
	}

private:

	bool m_mainTower = false;
	Collider m_coll{};

	uint32_t m_health;//uninit

	void JustDied();

};
