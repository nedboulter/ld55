#pragma once

#include "Obj.hpp"
#include "Util.hpp"

class BloodParticle : public Obj
{
public:


	BloodParticle(const Pos& p, const Pos& velocity)
		: Obj(TYPE_BLOODPARTICLE, p)
		, m_vel(velocity)
	{
		m_reg = (int)(Util::rand()*.999f * nRegs);
		m_lifetime = (int)(Util::rand() * 120);
	}

	inline void Update() override
	{

		const float sz = .25f;
		m_scrnBounds = MakeScrnBounds(m_pos,
			regs[m_reg].w * sz, regs[m_reg].h * sz, 0, 0);

		float frcX{}, frcY{}, frcZ{};
		
		// grv
		frcY -= .1f;

		// bounce
		if (m_pos.y < 0)
			frcY += abs(m_vel.y) + .1f;

		// drag
		m_vel.x *= .95f;
		m_vel.y *= .95f;
		m_vel.z *= .95f;

		if (m_lifetime-- < 0)
		{
			Remove();
		}

		m_vel.x += frcX;
		m_vel.y += frcY;
		m_vel.z += frcZ;

		m_pos.x += m_vel.x;
		m_pos.y += m_vel.y;
		m_pos.z += m_vel.z;


	}

	inline void Render(uint32_t* pScrn) override
	{

		RenderAsBillboard(pScrn,
			regs[m_reg],
			m_scrnBounds);

	}


private:

	// Random parts from the zombie :D
	static constexpr TexRegion regs[] =
	{
		{85, 90, 3, 4},
		{118, 90, 4, 4},
		{138, 90, 6, 6},
		{122, 104, 5, 3}
	};
	static constexpr int nRegs = _countof(regs);

	Pos m_vel;

	int m_reg{};

	int m_lifetime{};

};
