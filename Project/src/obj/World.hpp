#pragma once

#include "obj/Obj.hpp"
#include "obj/Coll.hpp"
#include "obj/Tower.hpp"

enum : uint32_t
{

	// value of a coin
	GOLDCOST_COIN = 10,

	// 1 coin 1 troop
	GOLDCOST_TROOP = 10,

	// 100 troops 1 tower
	GOLDCOST_TOWER = 1000,

	// GOLDCOST_GEN_... values are
	// the amount of gold is produced by events
	// per-second (nb: not per second
	// just whatever I changed it to trying
	// to balance last minute)

	// main tower produces 1 troop per-second
	GOLDCOST_GEN_MAINTOWER = GOLDCOST_TROOP * 3,

	// a built tower produces 3 troops per-second
	GOLDCOST_GEN_TOWER = GOLDCOST_TROOP,

	// If a troop kills a zombie we should get at least a troop back
	GOLDCOST_GEN_ZOMBIEKILL_MIN = GOLDCOST_TROOP,

	GOLDCOST_GEN_ZOMBIEKILL_MAX = GOLDCOST_TROOP * 5


};

class Camera
{
public:

	Camera()
		: x(0), y(20), z(0)
	{
		Pitch(75.f * ((float)M_PI / 180.f));
		Yaw(0.f);
	}

	inline float X() { return x; }
	inline float Y() { return y; }
	inline float Z() { return z; }
	inline float Pitch() { return pitch; }
	inline float Yaw() { return yaw; }

	inline void X(float x) { this->x = x; }
	inline void Y(float y) { this->y = y; }
	inline void Z(float z) { this->z = z; }

	inline void Pitch(float pitch)
	{
		this->pitch = pitch;

		sPitch = sinf(pitch);
		cPitch = cosf(pitch);
		forwardDirMat[0] = FLT_MAX;
	}
	inline void Yaw(float yaw)
	{
		this->yaw = yaw;

		sYaw = sinf(yaw);
		cYaw = cosf(yaw);
		forwardDirMat[0] = FLT_MAX;
	}

	inline float* GetFwdMatrix()
	{
		UpdateForward();

		return forwardDirMat;

	}

	inline float* GetOrientMat()
	{
		UpdateForward(); // updates both...

		return orientMat;
	}

	inline float SinPitch() { return sPitch; }
	inline float CosPitch() { return cPitch; }

	inline float SinYaw() { return sYaw; }
	inline float CosYaw() { return cYaw; }

private:

	float x, y, z;
	float pitch, yaw;
	float sPitch, cPitch;
	float sYaw, cYaw;

	float forwardDirMat[9]{FLT_MAX};
	float orientMat[9];

	inline void UpdateForward()
	{
		if (forwardDirMat[0] == FLT_MAX)
		{
			float xRot[9] =
			{
				1, 0, 0,
				0, cPitch, -sPitch,
				0, sPitch, cPitch
			};

			float yRot[9] =
			{
				cYaw, 0, sYaw,
				0, 1, 0,
				-sYaw, 0, cYaw
			};

			multMat<3>(yRot, xRot, forwardDirMat);

			// Inverse (just transpose, just rotation)
			for (int y = 0; y < 3; ++y)
			{
				for (int x = 0; x < 3; ++x)
				{
					orientMat[x + y * 3] = forwardDirMat[y + x * 3];
				}
			}

		}
	}

};

// grid goes from -300, 300 on x and z
const uint32_t uWORLDGRID_WIDTH = 100;
const uint32_t uWORLDGRID_HEIGHT = 100;

class GridEntry
{
public:
	static constexpr uint32_t uMAX_OBJS_PER_GRID_CELL = 128;

	Obj* m_objs[uMAX_OBJS_PER_GRID_CELL];
	uint32_t m_objCount;
};

class Character;

class World
{
public:

	World();
	CLASS_NO_COPY(World);

	void Tick();

	void Render(uint32_t* pScrn);

	template<typename T, typename ...ConstructorParams>
	inline T* NewGameObject(ConstructorParams&&... params)
	{
		static_assert(sizeof(T) < uMAX_GAMEOBJ_SIZE, "Game object too big!!");

		// Alloc
		const auto alloc = m_gameObjectsMemory.Alloc();

		// Create new object in mem
		T* objPtr = new (alloc.pBlock->data) T(std::forward<ConstructorParams>(params)...);

		// Add to add queue
		m_gameObjectsToAdd.push_back(std::make_pair(static_cast<Obj*>(objPtr), alloc));
		
		//m_gameObjects.insert(std::make_pair(static_cast<Obj*>(objPtr), alloc));

		return objPtr;
	}

	inline void DestroyGameObject(Obj* & pObj)
	{
		auto it = m_gameObjects.find(pObj);
		if (it == m_gameObjects.end())
			DBGERR("Game object destroyed not created!");

		// Destroy (use this instead of destructor)
		pObj->Destroy();

		// Free
		m_gameObjectsMemory.Free((*it).second);

		m_gameObjects.erase(it);
	}

	// Space conversions
	inline Pos WorldToView(const Pos& world)
	{
		// Camera stuff
		Pos ctrd =
		{
			// ctr at cam
			world.x - m_camera.X(),
			world.y - m_camera.Y(),
			world.z - m_camera.Z()
		};

		Pos res; // uninit

		// orient
		transformVector<3>(m_camera.GetOrientMat(), (float*)&ctrd, (float*)&res);

		return res;
	}

	inline void ViewToScreen(const Pos& view,
		float& scrX,
		float& scrY)
	{
		float xx = ((view.x) / view.z);
		float yy = -(view.y * (WIDTH / (float)HEIGHT)) / view.z;

		scrX = (WIDTH * (xx * .5f + .5f));
		scrY = (HEIGHT * (yy * .5f + .5f));
	}

	inline void WorldToScreen(const Pos& world,
		float& scrX,
		float& scrY,
		float* pViewZ = nullptr)
	{
		Pos view = WorldToView(world);
		ViewToScreen(view, scrX, scrY);

		if (pViewZ) *pViewZ = view.z;
	}

	Pos ScreenToWorldPosition(float scrX, float scrY, float* pW = nullptr);

	const Collider& GetFrustumBounds() { return m_frustumBounds; }

	inline Pos& GetCursorWorldPos() { return m_cursorWorldPos; }

	inline GridEntry* GetGridEntry(int gx, int gz)
	{
		
		gx += 50;
		gz += 50;

		if (gx > 0 && gz > 0 && gx < (int)uWORLDGRID_WIDTH && gz < (int)uWORLDGRID_HEIGHT)
		{
			return &m_grid[gx + gz * uWORLDGRID_WIDTH];
		}

		return nullptr;

	}

	inline Camera& GetCam() { return m_camera; }

	inline int GetGold() { return m_gold; }
	inline void AddGold(int g)
	{
		m_gold += g;
	}
	inline void RemoveGold(int g)
	{
		if (m_gold > g)
			m_gold -= g;
		else
			m_gold = 0;
	}

	inline bool CursorInBounds()
	{
		return m_cursorWorldPosInBounds;
	}

	// returns false if tower already there
	// doesn't handle gold etc.
	bool TryCreateNewTower(const Pos& p);

	inline uint32_t GetNumTroopsAroundCursor() { return m_numTroopsAroundCursor; }

	inline const std::vector<Tower*>& GetTowerList() { return m_towerList; }

	inline void RemoveTower(Tower* t)
	{
		auto end = std::remove(m_towerList.begin(), m_towerList.end(), t);
		m_towerList.erase(end, m_towerList.end());
	}

	inline void GameOver() { m_gameOver = true; }
	inline bool IsGameOver() { return m_gameOver; }

	inline void AddToScore(uint32_t u) { m_score += u; }

private:

	Camera m_camera;

	ObjectList m_gameObjectsMemory;
	std::unordered_map<Obj*, ObjectList::ObjAlloc> m_gameObjects;

	Collider m_frustumBounds;
	std::vector<Obj*> m_gameObjectsOnScreen;

	std::vector<std::pair<Obj*, ObjectList::ObjAlloc>> m_gameObjectsToAdd;
	std::vector<Obj*> m_gameObjectsToRemove;

	std::vector<GridEntry> m_grid;

	// if we need to reference an Obj
	// can add to the entry here (refCount)
	// and it won't actually be removed till refs are gone
	// this is kinda a last minute thing to get
	// zombie troop targets working
	std::unordered_map<Obj*, int> m_referencedObjects;

	float m_camVelX{}, m_camVelZ{};
	Pos m_cursorWorldPos{};
	bool m_cursorWorldPosInBounds = false;
	bool m_rightCursorWasDownLastTick = false;

	// Tower
	Tower* m_pMainTower{};
	std::vector<Tower*> m_towerList;

	const char* m_tipText = nullptr;
	uint32_t m_tipTextCooldown = 0;
	int m_tipTextX{};
	int m_tipTextY{};

	// Troop
	int m_addTroopCooldown = 0;

	// Gold
	int m_gold = 0;

	// Spawn rate ticker
	int m_spawnRateTicker = 0;

	bool m_gameOver = false;

	uint32_t m_numTroopsAroundCursor = 0;

	uint32_t m_score = 0;

	bool m_spaceWasDown = false;
	bool m_camRaise = false;

	void RenderGround(uint32_t* pScrn);

	void DriveCamera();

	void MakeDataStructures();

	void CreateMainTower();

	void HandleActions();

	void RenderGUI(uint32_t* pScrn);

};
