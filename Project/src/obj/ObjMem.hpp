#pragma once

constexpr uint32_t uMAX_GAMEOBJ_SIZE = 256;

class ObjectHeap
{
public:

	struct Block
	{
		union
		{
			// Data when allocd
			char data[uMAX_GAMEOBJ_SIZE];

			// Next free when in free list
			Block* pNextFree;
		};
	};

	static constexpr uint32_t uMaxNumBlocks = 2048;
	Block m_blocks[uMaxNumBlocks];
	Block* m_pFreeList{};
	uint32_t m_uNumBlocks = 0; // num allocated

	ObjectHeap()
	{
		// Link-em-up
		m_pFreeList = &m_blocks[0];
		for (uint32_t u = 0; u < uMaxNumBlocks - 1; ++u)
		{
			m_blocks[u].pNextFree = &m_blocks[u + 1];
		}
		m_blocks[uMaxNumBlocks - 1].pNextFree = nullptr;
	}

	Block* Try_Alloc()
	{
		if (m_uNumBlocks >= uMaxNumBlocks)
			return nullptr;

#ifdef _DEBUG
		// very very bad
		if (!m_pFreeList) DBGERR("Memory allocator corrrrrupptteeed (I fucked it :( )!");
#endif

		Block* pAlloc = m_pFreeList;
		m_pFreeList = m_pFreeList->pNextFree;

		++m_uNumBlocks;

		return pAlloc;
	}

	uint32_t BlocksLeft() { return uMaxNumBlocks - m_uNumBlocks; }

	void Free(Block* b)
	{
		if (!b) return;

		// if 'b' wasn't on this heap we're totally screwed
		Block* pFreeList = m_pFreeList;
		m_pFreeList = b;
		m_pFreeList->pNextFree = pFreeList;
		--m_uNumBlocks;

	}

};

class ObjectList
{
public:
	struct ObjAlloc
	{
		uint32_t heapIdx;
		ObjectHeap::Block* pBlock;
	};

	ObjectList()
	{
		newHeap();

	}

	ObjAlloc Alloc()
	{
		if (!m_hotHeap) DBGERR("fuckup");

		if (m_heapsCrapness > 30)
		{
			// If we switch heaps three times without
			// successful allocs then make a new heap
			newHeap();
		}

		ObjectHeap::Block* pBlock = m_hotHeap->Try_Alloc();

		if (pBlock)
		{

			// doing well!
			m_heapsCrapness = m_heapsCrapness > 0 ? m_heapsCrapness - 1 : 0;

			return
			{
				m_hotHeapIdx,
				pBlock
			};
		}
		else
		{
			findHeapWithSpace();

			ObjectHeap::Block* pBlock = m_hotHeap->Try_Alloc();
			if (!pBlock) DBGERR("fuckup");

			return
			{
				m_hotHeapIdx,
				pBlock
			};

		}

	}

	void Free(const ObjAlloc& a)
	{
		m_heaps[a.heapIdx]->Free(a.pBlock);
	}

	~ObjectList()
	{
		for (ObjectHeap* pHeap : m_heaps)
		{
			delete pHeap;
		}

		m_heaps.clear();
	}

private:

	ObjectHeap* m_hotHeap{};
	uint32_t m_hotHeapIdx{};

	uint32_t m_heapsCrapness = 0;

	std::vector<ObjectHeap*> m_heaps;

	void findHeapWithSpace()
	{
		for (uint32_t i = 0; i < (uint32_t)m_heaps.size(); ++i)
		{
			if (m_heaps[i]->BlocksLeft() > 0)
			{
				m_hotHeap = m_heaps[i];
				m_hotHeapIdx = i;
				m_heapsCrapness += 10;
				return;
			}
		}

		// All full
		newHeap();
	}

	void newHeap()
	{
		m_hotHeap = new ObjectHeap();
		m_heaps.push_back(m_hotHeap);
		m_hotHeapIdx = (uint32_t)m_heaps.size() - 1;
		m_heapsCrapness = 0;
	}

};
