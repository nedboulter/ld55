#include "pch.hpp"
#include "World.hpp"

#include "Util.hpp"
#include "Game.hpp"
#include "Textures.hpp"
#include "Tree.hpp"
#include "Character.hpp"
#include "Snd.hpp"
#include "Gold.hpp"
#include "Text.hpp"

World::World()
{

	float treeRegionStart = -300.f;
	float treeRegionSz = 600.f;
	uint32_t uNumTrees = 8192*2;

	Textures& tex = Game::Get().GetTextures();

	for (uint32_t u = 0; u < uNumTrees; ++u)
	{
		
		float x = Util::rand() * treeRegionSz + treeRegionStart;
		float z = Util::rand() * treeRegionSz + treeRegionStart;

		int type =
			Util::rand() < .9f ?
			2 /*grass*/ :

			Util::rand() < .5f ? 0 : 1 /* tree */;

		NewGameObject<Tree>(Pos{ x, 0, z }, type);
		

	}

	// required so CreateMainTower knows
	// what elements to remove (main tower tile shouldn't have trees)
	MakeDataStructures();

	CreateMainTower();

}

void World::Tick()
{
	Game& g = Game::Get();

	// Move camera around,
	// cursor is handled by game
	DriveCamera();

	// - Place troops
	// - Place tower
	// picking up gold,
	// and leading are handled
	// by those respective objects
	if (!m_gameOver)
	{
		HandleActions();

		bool spaceDown = g.KeyDown(VK_SPACE);
		if (!m_spaceWasDown && spaceDown)
		{
			m_camRaise = !m_camRaise;
		}

		m_spaceWasDown = spaceDown;

	}

	// TODO remove debug stuff
#if 0
	{
		static bool bWasDown = false;
		bool isDown = g.MouseDown();
		bool clicked = !bWasDown && isDown;
		bWasDown = isDown;

		if (isDown)
		{
			const Pos& p = GetCursorWorldPos();
			DBGLOG("Click Pos {}, {}, {}", p.x, p.y, p.z);

			if (g.KeyDown(VK_F1))
				NewGameObject<Tree>(p, (int)(Util::rand() * 3));

			if (g.KeyDown(VK_F2))
				NewGameObject<Character>(p, CHARTYPE_TROOP);

			if (g.KeyDown(VK_F3))
				NewGameObject<Character>(p, CHARTYPE_ZOMBIE);

			if (g.KeyDown(VK_F4))
			{
				int cgx = (int)std::floor(p.x / 10.f);
				int cgz = (int)std::floor(p.z / 10.f);

				auto* e = GetGridEntry(cgx, cgz);
				uint32_t n = e ? e->m_objCount : UINT_MAX;
				DBGLOG("{} entities in gridcell ({},{})", n, cgx, cgz);
			}


			if (g.KeyDown(VK_F5) && clicked)
			{
				NewGameObject<Tower>(p, true);
			}

			if (g.KeyDown(VK_F6) && clicked)
			{
				NewGameObject<Tower>(p, false);
			}

			if (g.KeyDown(VK_F7) && clicked)
			{
				Game::Get().GetSnd().Play((Snds)(int)(Util::rand() * (float)SND_COUNT));
			}

			if (g.KeyDown(VK_F8))
			{
				NewGameObject<Gold>(p);
			}

		}
	}
#endif

	MakeDataStructures();

	// Clear on-screen list
	m_gameObjectsOnScreen.clear();

	uint32_t cullCount = 0;

	for (const auto& objEntry : m_gameObjects)
	{
		Obj* o = objEntry.first;
		o->Tick();

		// On Scrn list
		if (o->GetScrnBounds().OnScreen())
			m_gameObjectsOnScreen.push_back(o);
		else
			++cullCount;

		// Removal list
		if (o->GetFlags() & Obj::FLAG_WANTSREMOVAL)
		{
			m_gameObjectsToRemove.push_back(o);
		}
	}

	// Sort game objects on screen based on depth
	std::sort(m_gameObjectsOnScreen.begin(), m_gameObjectsOnScreen.end(),
		[](const Obj* pA, const Obj* pB)
		{
			return pA->GetScrnBounds().viewZ > pB->GetScrnBounds().viewZ;
		});

	if (m_tipTextCooldown > 0)
		--m_tipTextCooldown;

	m_rightCursorWasDownLastTick = g.RightMouseDown();

}

void World::Render(uint32_t* pScrn)
{

	RenderGround(pScrn);

	for (Obj* o : m_gameObjectsOnScreen)
	{
		o->Render(pScrn);
	}

	RenderGUI(pScrn);

}

static void GroundRay(
	Camera& cam,
	float pixX,
	float pixY,
	float& out_groundX,
	float& out_groundZ,
	float& out_w)
{
	// Ray through pix
	float a[3] =
	{
		(pixX / (float)WIDTH * 2 - 1),
		-(pixY / (float)HEIGHT * 2 - 1) / (WIDTH / (float)HEIGHT),
		1
	};
	
	// Orient by cam dir
	float b[3];
	transformVector<3>(cam.GetFwdMatrix(), a, b);

	// Norm to y
	b[0] /= b[1];
	b[2] /= b[1];
	b[1] = 1.f;

	// Scale by y-dist from plane
	b[0] *= cam.Y();
	b[1] *= cam.Y();
	b[2] *= cam.Y();

	// Result
	out_groundX = -(b[0] - cam.X());
	out_groundZ = -(b[2] - cam.Z());

	// To compute W we need the raylen
	float xDiff = b[0];
	float yDiff = cam.Y();
	float zDiff = b[2];

	float len = sqrt(xDiff*xDiff + yDiff*yDiff + zDiff*zDiff);
	
	// normalize a
	float aLen = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
	float aZ = 1.f / aLen;

	out_w = aZ * len;

}

inline float lerp(float a, float b, float t)
{
	return a + t * (b - a);
}

Pos World::ScreenToWorldPosition(float scrX, float scrY, float* pW)
{
	float gx, gz, w;
	GroundRay(m_camera,
		scrX, scrY, gx, gz, w);

	if (pW) *pW = w;

	return { gx, 0, gz };
}

bool World::TryCreateNewTower(const Pos& p)
{
	int gx = (int)std::floor(p.x / 10.f);
	int gz = (int)std::floor(p.z / 10.f);

	GridEntry* ge = GetGridEntry(gx, gz);
	
	if (ge)
	{

		bool isAlreadyTower = false;
		for (uint32_t u = 0; u < ge->m_objCount; ++u)
		{
			Obj* o = ge->m_objs[u];

			if (o->m_type == Obj::TYPE_TOWER)
			{
				isAlreadyTower = true;
				break;
			}
		}

		if (!isAlreadyTower)
		{
			// Remove/kill objects that need to be removed/killed
			for (uint32_t u = 0; u < ge->m_objCount; ++u)
			{
				Obj* o = ge->m_objs[u];

				if (o->m_type == Obj::TYPE_CHARACTER)
				{
					// Kill any characters that were standing under the tower :D
					Character* chr = (Character*)o;
					chr->Damage(nullptr, chr->GetHealth());
				}
				else
				{
					// Basically remove trees and grass..
					if (o->m_type != Obj::TYPE_GOLD && o->m_type != Obj::TYPE_BLOODPARTICLE)
						o->Remove();
				}

			}

			// Place
			Pos ctrPos = { gx*10 + 5.f, 0, gz*10 + 5.f };
			m_towerList.push_back(NewGameObject<Tower>(ctrPos, false));
			Game::Get().GetSnd().Play(SND_CREATETOWER);
			return true;

		}

	}

	return false;

}

void World::RenderGround(uint32_t* pScrn)
{

	Textures& tex = Game::Get().GetTextures();

	int curGX = (int)std::floor(m_cursorWorldPos.x / 10.f);
	int curGZ = (int)std::floor(m_cursorWorldPos.z / 10.f);

	float* fwdMat = m_camera.GetFwdMatrix();

	float tlgX, tlgZ, tlgW;
	GroundRay(m_camera, 0, 0, tlgX, tlgZ, tlgW);

	float trgX, trgZ, trgW;
	GroundRay(m_camera, WIDTH - 1, 0, trgX, trgZ, trgW);

	float blgX, blgZ, blgW;
	GroundRay(m_camera, 0, HEIGHT-1, blgX, blgZ, blgW);

	float brgX, brgZ, brgW;
	GroundRay(m_camera, WIDTH - 1, HEIGHT - 1, brgX, brgZ, brgW);

	// will vary linearly in screen space
	tlgX /= tlgW;
	tlgZ /= tlgW;
	tlgW = 1.f / tlgW;
	
	trgX /= trgW;
	trgZ /= trgW;
	trgW = 1.f / trgW;

	blgX /= blgW;
	blgZ /= blgW;
	blgW = 1.f / blgW;

	brgX /= brgW;
	brgZ /= brgW;
	brgW = 1.f / brgW;

	for (int y = 0; y < HEIGHT; ++y)
	{
		float yy = y / (float)HEIGHT;

		// Vertical lerps
		float leftX = lerp(tlgX, blgX, yy);
		float leftZ = lerp(tlgZ, blgZ, yy);
		float leftW = lerp(tlgW, blgW, yy);

		float rightX = lerp(trgX, brgX, yy);
		float rightZ = lerp(trgZ, brgZ, yy);
		float rightW = lerp(trgW, brgW, yy);


		for (int x = 0; x < WIDTH; ++x)
		{
			float xx = x / (float)WIDTH;

			// Horizontal lerps (this is bilinear interpolation wooow)
			float X = lerp(leftX, rightX, xx);
			float Z = lerp(leftZ, rightZ, xx);
			float W = lerp(leftW, rightW, xx);

			// persp correct stuff
			W = 1.f / W;

			X *= W;
			Z *= W;

			//float gridX = std::floor(X / 10.f);
			//float gridZ = std::floor(Z / 10.f);
			//
			//uint32_t col = (tex.Noise(X * .1f, Z * .1f) / 2);
			//
			//if (m_cursorWorldPos.x > gridX && m_cursorWorldPos.z > gridZ &&
			//	m_cursorWorldPos.x < gridX + 10 && m_cursorWorldPos.z < gridZ + 10)
			//{
			//	col = std::min(255u, col + col / 2);
			//}

			uint32_t col = (tex.Noise(X * .1f, Z * .1f) / 2);

			if (CursorInBounds())
			{
				int gridX = (int)std::floor(X / 10.f);
				int gridZ = (int)std::floor(Z / 10.f);

				if (gridX == curGX && gridZ == curGZ)
				{
					col = std::min(255u, col + col / 2);
				}
			}

			col = col << 8;

			//float xDiff = X - m_cursorWorldPos.x;
			//float zDiff = Z - m_cursorWorldPos.z;
			//float len = sqrt(xDiff * xDiff + zDiff * zDiff);
			//
			//if (len < .5f)
			//{
			//	col = 0xff00ff;
			//}

			pScrn[x + y * WIDTH] = col;

		}

	}

}

void World::DriveCamera()
{

	Game& g = Game::Get();

	Cursor& c = g.GetCursor();

	float cx = c.x / (float)WIDTH;
	float cy = c.y / (float)HEIGHT;

	float pitchStart = 50.f * ((float)M_PI / 180.f);
	float pitchEnd = 60.f * ((float)M_PI / 180.f);
	m_camera.Pitch(lerp(pitchStart, pitchEnd, cy));

	float yawStart = -10.f * ((float)M_PI / 180.f);
	float yawEnd = 10.f * ((float)M_PI / 180.f);
	m_camera.Yaw(lerp(yawStart, yawEnd, cx));

	float xDir{}, zDir{};

	if (!m_gameOver && !g.KeyDown('Q') && !g.KeyDown('E'))
	{
		if (g.KeyDown('W'))
			zDir += 1;
		if (g.KeyDown('S'))
			zDir -= 1;
		if (g.KeyDown('A'))
			xDir -= 1;
		if (g.KeyDown('D'))
			xDir += 1;
	}
	else
	{
		float tgtX = m_pMainTower->GetPos().x;
		float tgtZ = m_pMainTower->GetPos().z - 20.f;

		xDir = tgtX - m_camera.X();
		zDir = tgtZ - m_camera.Z();

	}

	float dirLen = sqrt(xDir * xDir + zDir * zDir);
	if (dirLen > .01f)
	{
		const float camMoveSpd = .25f;

		m_camVelX += xDir / dirLen * camMoveSpd;
		m_camVelZ += zDir / dirLen * camMoveSpd;

	}

	m_camVelX *= .8f;
	m_camVelZ *= .8f;

	if (m_camVelX < 0 && m_camera.X() < -300)
		m_camVelX = 0;

	if (m_camVelX > 0 && m_camera.X() > 300)
		m_camVelX = 0;

	if (m_camVelZ < 0 && m_camera.Z() < -300)
		m_camVelZ = 0;

	if (m_camVelZ > 0 && m_camera.Z() > 300)
		m_camVelZ = 0;

	m_camera.X(m_camera.X() + m_camVelX);
	m_camera.Z(m_camera.Z() + m_camVelZ);

	float camY = 20;
	if (m_camRaise)
	{
		camY = 40;
	}

	float t = (m_camera.Y() - 20) / (40 - 20);
	float spd = .5f + sinf(t) * 2.f;

	if (m_camera.Y() < camY - spd)
	{
		m_camera.Y(m_camera.Y() + spd);
	}
	else if (m_camera.Y() > camY + spd)
	{
		m_camera.Y(m_camera.Y() - spd);
	}
	else
	{
		m_camera.Y(camY);
	}

	// Get the cursor position with the new cam params
	m_cursorWorldPos = ScreenToWorldPosition(g.GetCursor().x, g.GetCursor().y);

	m_cursorWorldPosInBounds = m_cursorWorldPos.x > -300 && m_cursorWorldPos.z > -300 &&
		m_cursorWorldPos.x < 300 && m_cursorWorldPos.z < 300;
}

void World::MakeDataStructures()
{

	// Add/remove game objects added/removed last frame
	for (const auto& p : m_gameObjectsToAdd)
	{
		m_gameObjects.insert(p);
	}

	for (Obj* o : m_gameObjectsToRemove)
	{
		
		DestroyGameObject(o);
	}

	m_gameObjectsToAdd.clear();
	m_gameObjectsToRemove.clear();

	// Clear grid (yes this could've been used to work out what's on screen...)
	if (m_grid.empty())
	{
		m_grid.resize(uWORLDGRID_WIDTH * uWORLDGRID_HEIGHT);
	}
	memset(m_grid.data(), 0, sizeof(GridEntry) * uWORLDGRID_WIDTH * uWORLDGRID_HEIGHT);

	// Build grid
	for (const auto& e : m_gameObjects)
	{
		Obj* o = e.first;

		// Grid list
		if (o->GetFlags() & Obj::FLAG_WANTSGRID)
		{
			const auto& p = o->GetPos();
			int gx = (int)std::floor(p.x / 10.f);
			int gz = (int)std::floor(p.z / 10.f);

			GridEntry* pE = GetGridEntry(gx, gz);
			if (!pE)
			{
				DBGLOG("Entity outside of grid!");
				o->Remove();
			}
			else
			{
				if (pE->m_objCount < GridEntry::uMAX_OBJS_PER_GRID_CELL)
				{
					// add to grid
					Obj*& obj = pE->m_objs[pE->m_objCount++];
					obj = o;
				}
			}

		}

	}

}

void World::CreateMainTower()
{

	auto KillEntry = [this](int gx, int gz)
	{
		GridEntry* ge = GetGridEntry(gx, gz);
		if (ge)
		{
			for (uint32_t u = 0; u < ge->m_objCount; ++u)
			{
				ge->m_objs[u]->Remove();
			}
		}
	};

	Pos p = { 5, 0, 5 };

	// what loops are for
	KillEntry(-1, -1);
	KillEntry(0, -1);
	KillEntry(1, -1);

	KillEntry(-1, 0);
	KillEntry(0, 0);
	KillEntry(1, 0);

	KillEntry(-1, 1);
	KillEntry(0, 1);
	KillEntry(1, 1);

	m_pMainTower = NewGameObject<Tower>(p, true);
	m_towerList.push_back(m_pMainTower);

}

void World::HandleActions()
{
	Game& g = Game::Get();

	// Place tower
	{

		const bool modifier = g.KeyDown(VK_LSHIFT) || g.KeyDown(VK_LCONTROL);

		const bool bRightClick = !m_rightCursorWasDownLastTick && g.RightMouseDown();

		if ( modifier && bRightClick && m_pMainTower)
		{
			const Pos& cWorldPos = GetCursorWorldPos();

			float distFromMainTower;
			{
				float xDiff = cWorldPos.x - m_pMainTower->GetPos().x;
				float zDiff = cWorldPos.z - m_pMainTower->GetPos().z;
				distFromMainTower = sqrt(xDiff * xDiff + zDiff * zDiff);
			}

			if (CursorInBounds())
			{
				if (m_gold > GOLDCOST_TOWER)
				{
					if (distFromMainTower < 30)
					{
						m_tipTextX = WIDTH / 2 - 100;
						m_tipTextY = HEIGHT / 2 - 10;
						m_tipTextCooldown = 120;
						m_tipText = "Cannot place a tower\nthis close to the main tower!";


						Snd& snd = g.GetSnd();
						snd.Play(SND_HIT);
					}
					else
					{
						if (!TryCreateNewTower(cWorldPos))
						{
							m_tipTextX = WIDTH / 2 - 75;
							m_tipTextY = HEIGHT / 2 - 25;
							m_tipTextCooldown = 120;
							m_tipText = "Cannot place a tower\nwhere there is already a tower!\n(even if destroyed)";

							Snd& snd = g.GetSnd();
							snd.Play(SND_HIT);
						}
						else
						{
							RemoveGold(GOLDCOST_TOWER);
						}
					}
				}
				else
				{
					m_tipTextX = WIDTH / 2 - 100;
					m_tipTextY = HEIGHT / 2 - 10;
					m_tipTextCooldown = 120;
					m_tipText = "Cannot afford to place a tower!";

					Snd& snd = g.GetSnd();
					snd.Play(SND_HIT);
				}
			}
		}
	}


	{ // Place troops

		if (g.RightMouseDown() && CursorInBounds())
		{

			if (m_addTroopCooldown <= 0)
			{

				if (m_gold > GOLDCOST_TROOP)
				{
					NewGameObject<Character>(GetCursorWorldPos(), CHARTYPE_TROOP);
					g.GetSnd().Play(SND_LOWHEALTH, .01f);

					RemoveGold(GOLDCOST_TROOP);

					// removed the cooldown, it was lame :D
					// 60 troops per second! Yay!
					m_addTroopCooldown = 0;
				}
				else
				{
					g.GetSnd().Play(SND_HIT);
				}
			}

		}

		// Num troops around cursor
		const Pos& p = GetCursorWorldPos();
		int gx = (int)std::floor(p.x / 10);
		int gz = (int)std::floor(p.z / 10);

		int a = 1;

		m_numTroopsAroundCursor = 0;

		for (int zz = gz - a; zz <= gz + a; ++zz)
		{
			for (int xx = gx - a; xx <= gx + a; ++xx)
			{
				GridEntry* ge = GetGridEntry(xx, zz);

				if (ge)
				{

					for (uint32_t u = 0; u < ge->m_objCount; ++u)
					{
						if (ge->m_objs[u]->m_type == Obj::TYPE_CHARACTER)
						{
							Character* chr = (Character*)ge->m_objs[u];
							if (chr->charType == CHARTYPE_TROOP)
								++m_numTroopsAroundCursor;
						}
					}
				}
			}
		}

		

		--m_addTroopCooldown;

	}


	// Spawn Zombies
	{

		float zombiesPerSecondEasy = 2;
		float zombiesPerSecondHard = 100;

		// Scales ticks so that 10 minutes of gameplay is T = 1
		float T = m_spawnRateTicker * (1.f / (10*60*60));

		// Difficulty Curve
		float D = T*T*T;
		
		// D should be 1 at hard, and 0 at easy
		float zombiesPerSecond = (zombiesPerSecondHard - zombiesPerSecondEasy) * D + zombiesPerSecondEasy;

		float zombiesPerTick = zombiesPerSecond / 60.f;

		if (Util::rand() < zombiesPerTick)
		{
			float rx = Util::rand() * 600 - 300;
			float rz = Util::rand() * 600 - 300;

			NewGameObject<Character>(Pos{ rx, 0, rz }, CHARTYPE_ZOMBIE);
		}

		m_spawnRateTicker++;
	}

}

void World::RenderGUI(uint32_t* pScrn)
{

	Textures& t = Game::Get().GetTextures();

	Text::Write(std::format("{}", m_gold), 35, 10, 0xdfdf00);

	t.Blit(pScrn,
		{ 65, 125, 20, 20 },
		10, 7);

	if (m_tipTextCooldown > 0)
	{
		Text::Write(m_tipText, m_tipTextX, m_tipTextY, 0x7f7f7f);
	}

	if (m_pMainTower)
	{

		Text::Write(std::format("{}", m_pMainTower->GetHealth()), 35, 38, 0xdfdf00);

		t.Blit(pScrn,
			{ 15, 136, 18, 14 },
			10, 40);

	}

	Text::Write(std::format("Score {}", m_score), 10, HEIGHT - 20, 0xdfdf00);

	if (m_gameOver)
	{
		Text::Write("Game Over!", WIDTH / 2 - 60, HEIGHT / 2 - 10, 0xff0000);
	}

}
