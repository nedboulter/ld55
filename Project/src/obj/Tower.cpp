#include "pch.hpp"
#include "Tower.hpp"

#include "World.hpp"
#include "Util.hpp"
#include "Gold.hpp"

void Tower::JustDied()
{
	// Erase from tower list
	Game::Get().GetWorld()->RemoveTower(this);
	Game::Get().GetSnd().Play(SND_DEATHTOWER);

	if (m_mainTower)
		Game::Get().GetWorld()->GameOver();
}

void Tower::Update()
{

	uint32_t pixH = m_mainTower ? 128 : 102;
	float propOffs = -.4f;

	const float sz = .15f;
	m_scrnBounds = MakeScrnBounds(
		m_pos,
		48 * sz, pixH * sz,
		0, propOffs
	);

	m_coll = Collider::FromPos(m_pos, (48 * sz) / 2.f,
		(pixH * sz) / 4.f);

	if (!IsDead())
	{

		if (Util::rand() < .5f / 60.f)
		{
			int coins = (int)(m_mainTower ? GOLDCOST_GEN_MAINTOWER : GOLDCOST_GEN_TOWER) / (int)GOLDCOST_COIN;

			for (int i = 0; i < coins; ++i)
			{
				Game::Get().GetWorld()->NewGameObject<Gold>(m_pos);
			}
		}

		if (!Game::Get().GetWorld()->IsGameOver())
		{
			Game::Get().GetWorld()->AddToScore(1);
		}

	}

}

