#pragma once

#include "obj/Obj.hpp"
#include "Textures.hpp"
#include "Game.hpp"

class Tree : public Obj
{
public:
	using Parent = Obj;

	Tree(const Pos& pos, int type)
		: Obj( TYPE_TREE, pos )
		, m_type(type)
	{
		// first 2 are trees,
		// grass doesn't need to be in the grid
		//if (m_type == 0 || m_type == 1)
			m_uFlags |= FLAG_WANTSGRID;
	}

	inline void Update()
	{
		const float sz = .25f;
		m_scrnBounds = MakeScrnBounds(
			m_pos,
			regs[m_type].w * sz,
			regs[m_type].h * sz,
			0, -.5f);
	}

	inline void Render(uint32_t* pScrn)
	{

		RenderAsBillboard(pScrn,
			regs[m_type],
			m_scrnBounds);

		//Game& g = Game::Get();
		//Textures& t = g.GetTextures();
		//World* world = g.GetWorld();
		//
		//float scrX, scrY, z;
		//world->WorldToScreen(m_pos, scrX, scrY, &z);
		//
		//int pixW = 13, pixH = 14;
		//
		//float W = WIDTH/2.f, H = HEIGHT/2.f;
		//W /= z;
		//H /= z;
		//
		////constexpr float METERS_PER_PIX = 1 / 16.f;
		////float hw = (pixW / 2.f) * METERS_PER_PIX;
		////float h = pixH * METERS_PER_PIX;
		////
		////hw /= z;
		////h /= z;
		//
		//t.StretchBlit(pScrn,
		//	{ 2, 18, pixW, pixH},
		//	(int)(scrX - W/2.f),
		//	(int)(scrY - H),
		//	(int)W, (int)H);

	}

	inline Collider* GetCollider()
	{
		// grass
		if (m_type == 2)
		{
			return nullptr;
		}
		else
		{
			// Tree
			const float sz = .25f;
			m_coll = Collider::FromPos(m_pos,
				(16 * sz) / 2.f,
				(16 * sz) / 4.f);

			return &m_coll;
		}
	}

private:
	static constexpr const TexRegion regs[] =
	{
		{160, 64, 30, 32},
		{192, 50, 22, 46},
		{223, 86, 17, 10}
	};

	int m_type{};
	Collider m_coll;

};
