#include "pch.hpp"
#include "Character.hpp"

#include "Game.hpp"
#include "World.hpp"
#include "Textures.hpp"
#include "Util.hpp"

#include "obj/BloodParticle.hpp"
#include "obj/Gold.hpp"

Character::Character(const Pos& p, CharType charType)
	: Obj(TYPE_CHARACTER, p)
	, charType(charType)
{
	m_uFlags |= FLAG_WANTSGRID;

	// might want to do something interesting here?
	m_health = 100;

}

void Character::Update()
{
	
	m_frcX = 0;
	m_frcZ = 0;

	Obj* colliding[16];
	float gridComplexity{};
	uint32_t nColliding = GetCollisions(colliding, &gridComplexity);

	if (charType == CHARTYPE_TROOP)
		DoTroopLogic(gridComplexity, nColliding, colliding);
	else
		DoZombieLogic(nColliding, colliding);

	PushOutOfStuff(nColliding, colliding);

	// Force
	m_velX += m_frcX;
	m_velZ += m_frcZ;

	// Friction
	m_velX *= .8f;
	m_velZ *= .8f;

	DoAnim();

	// Move
	m_pos.x += m_velX;
	m_pos.z += m_velZ;

	// Bounds
	const float sz = .25f;

	m_coll = Collider::FromPos(m_pos, (16 * sz)/2.f, (16 * sz)/4.f);

	float shrinky = 1.f;
	if (m_hurtCooldown > 0)
	{
		// Red tint would make more sense,
		// we're shrinking the char to show he got hit
		shrinky = .5f;
		--m_hurtCooldown;
	}

	m_scrnBounds = MakeScrnBounds(
		m_pos,
		17 * sz, (16 * sz)*shrinky, 0, -.5f);

}

void Character::Render(uint32_t* pScrn)
{
	Textures& t = Game::Get().GetTextures();

	static Character* pChar = nullptr;

	constexpr uint32_t tPerFrame = (xAnimPeriod / numFrames);
	uint32_t frame = m_framesAnimTime / tPerFrame;

	uint32_t framesXOffs =
		frame * 17
		+ (charType == CHARTYPE_ZOMBIE ? 80 : 0);

	RenderAsBillboard(pScrn,
		{ (int)framesXOffs, 48 + m_framesYOffs*16, 17, 16 },
		m_scrnBounds);

#if 1
	//float sz = .5f;
	//RenderAsBillboard(pScrn,
	//	{ 16, 0, 5, 5 },
	//	MakeScrnBounds(m_pos, 5.f * sz, 5.f * sz, 0, 0));

	//RenderAsBillboard(pScrn, { 16, 0, 5, 5 },
	//	MakeScrnBounds({m_coll.minX, 0, m_coll.minZ}, .5f * .5f, .5f * .5f, 0, 0));
	//
	//RenderAsBillboard(pScrn, { 16, 0, 5, 5 },
	//	MakeScrnBounds({ m_coll.maxX, 0, m_coll.maxZ }, .5f * .5f, .5f * .5f, 0, 0));

#endif
}

#if 0
uint32_t Character::GetCollisions(
	Obj* (&outResult)[16])
{

	Collider* pThisColl = GetCollider();
	if (!pThisColl)
	{
		return 0;
	}

	uint32_t count = 0;

	World& w = *Game::Get().GetWorld();

	int gxStart = (int)std::floor(pThisColl->minX / 10.f);
	int gzStart = (int)std::floor(pThisColl->minZ / 10.f);

	int gxEnd = (int)std::ceil(pThisColl->maxX / 10.f);
	int gzEnd = (int)std::ceil(pThisColl->maxZ / 10.f);

	for (int z = gzStart; z < gzEnd; ++z)
	{
		for (int x = gxStart; x < gxEnd; ++x)
		{
			GridEntry* pCell = w.GetGridEntry(x, z);

			if (pCell)
			{

				for (uint32_t i = 0; i < pCell->m_objCount; ++i)
				{
					Obj* o = pCell->m_objs[i];

					if (o == this || !o) continue;

					Collider* c = o->GetCollider();
					if (c && c->Intersects(*pThisColl))
					{
						if (count < 16)
						{
							outResult[count++] = o;
						}
						else
						{
							DBGLOG("Out of colls");
						}
					}

				}

			}
			else
				DBGLOG("Grid coll check OOB!!");

		}
	}

	return count;

}
#endif

void Character::DoTroopLogic(
	float gridComplexity,
	uint32_t collCnt,
	Obj** ppColls)
{

	Game& g = Game::Get();
	World& w = *g.GetWorld();

	if (g.MouseDown() && w.CursorInBounds() && !g.KeyDown(VK_F2))
	{


		const Pos& p = w.GetCursorWorldPos();

		float xDiff = p.x - m_pos.x;
		float zDiff = p.z - m_pos.z;
		float len = sqrt(xDiff * xDiff + zDiff * zDiff);
		
		if (len > 5 && len < 10*3 && w.GetNumTroopsAroundCursor() < 30)
		{
			// Pull towards target
			float spd = .1f;
			
			m_frcX += xDiff / len * spd;
			m_frcZ += zDiff / len * spd;

		}
	}
}

void Character::DoZombieLogic(
	uint32_t collCnt,
	Obj** ppColls)
{

	World& w = *Game::Get().GetWorld();
	
	if (m_target)
	{
		// could get removed after this update,
		// so if that happens it will either have the old data
		// (so flag wantsremoval = true)
		// or we will have a new entity in that slot,
		// in which case the uuid will be different
		if (m_target->m_uuid != m_targetUUID || m_target->GetFlags() & FLAG_WANTSREMOVAL)
		{
			m_target = nullptr;
			m_targetUUID = UINT_MAX;
		}
		else
		{
			// If attacking a dead tower
			if (m_target->m_type == TYPE_TOWER && ((Tower*)m_target)->IsDead())
			{
				m_target = nullptr;
				m_targetUUID = UINT_MAX;
			}
		}
	}

	// Attack troops
	{

		// if we have no target, and there's a nearby troop,
		// we definitely want to attack it
		bool tryAttackTroop = !m_target;

		if (!tryAttackTroop)
		{
			if (m_target->m_type == Obj::TYPE_TOWER)
			{
				float xDiff = m_target->GetPos().x - m_pos.x;
				float zDiff = m_target->GetPos().z - m_pos.z;
				float len = sqrt(xDiff * xDiff + zDiff * zDiff);

				if (len > 50)
				{
					// if we're far away from our tower, we should probably
					// go attack a nearby troop, if there is one
					tryAttackTroop = true;
				}
				else
				{
					// it's good if they leave their target every now and then anyway
					// otherwise I'm worried they'll get stuck in the tower or something
					// this way you can distract them by making nearby troops
					const float probabilityOfGivingUpOnTowerPerSecond = .5f;
					const float perTick = probabilityOfGivingUpOnTowerPerSecond / 60.f;

					tryAttackTroop = Util::rand() < perTick;
				}
			}
		}

		if (tryAttackTroop)
		{
			// if we're not targetting, or we're targetting a tower, we
			// can have a look around for nearby troops

			int gx = (int)std::floor(m_pos.x / 10.f);
			int gz = (int)std::floor(m_pos.z / 10.f);

			Character* nearbyTroop{};

			for (int z = gz - 1; z < gz + 1; ++z)
			{
				for (int x = gx - 1; x < gx + 1; ++x)
				{
					GridEntry* ge = w.GetGridEntry(gx, gz);

					if (ge)
					{
						for (uint32_t u = 0; u < ge->m_objCount; ++u)
						{
							Obj* o = ge->m_objs[u];

							if (o->m_type == TYPE_CHARACTER)
							{
								Character* chr = (Character*)o;
								if (chr->charType == CHARTYPE_TROOP)
								{
									// grab first not closest, because
									// there might be *loads* of them
									// and i don't wanna go through them all

									nearbyTroop = chr;
									break;
								}
							}

						}
					}

				}
			}

			if (nearbyTroop)
			{

				// Switch target, so this troop might actually
				// die then it's gonna be pointing to some random object...
				m_target = nearbyTroop;
				m_targetUUID = m_target->m_uuid;

			}

		}

	}

	const auto& towers = w.GetTowerList();

	if (!m_target)
	{

		// I kinda like the 0 locality
		// means we'll have zombies walking around randomly
		// and also means it advantageous to
		// place towers in drastically different locations.

		if (!towers.empty())
		{
			int numTowers = (int)towers.size();
			int rIdx = (int)(Util::rand() * numTowers);

			Tower* randomTower = towers[rIdx];
			m_target = randomTower;
			m_targetUUID = randomTower->m_uuid;
		}

	}

	if (m_target)
	{

		// imagine if I had wrapped vectors somehow...
		float xDiff = m_target->GetPos().x - m_pos.x;
		float zDiff = m_target->GetPos().z - m_pos.z;
		float len = sqrt(xDiff * xDiff + zDiff * zDiff);

		if (len > .1f)
		{

			// Pull towards target
			float spd = .1f;

			m_frcX += xDiff / len * spd;
			m_frcZ += zDiff / len * spd;

		}

	}

}

void Character::PushOutOfStuff(
	uint32_t count,
	Obj** ppColliding)
{

	float pushOutStrength = .5f;

	for (uint32_t u = 0; u < count; ++u)
	{
		Obj* o = ppColliding[u];

		if (o->m_type == TYPE_CHARACTER)
		{
			// Try and avoid bunching between same-same chars
			pushOutStrength = .1f;

			Character* chr = (Character*)o;

			// If we're a zombie colliding with a troop, OR
			// if we're a troop colliding with a zombie, we
			// need to try and hit them
			if (charType == CHARTYPE_ZOMBIE && chr->charType == CHARTYPE_TROOP ||
				charType == CHARTYPE_TROOP && chr->charType == CHARTYPE_ZOMBIE)
			{
				DoDamageLogic(chr);

				// Softer so they can fight while intersecting
				pushOutStrength = .015f;
			}
		}
		else if (o->m_type == TYPE_GOLD)
		{
			// don't let gold push around chars
			pushOutStrength = 0;
		}
		else if (o->m_type == TYPE_TOWER)
		{

			Tower* t = (Tower*)o;

			if (charType == CHARTYPE_ZOMBIE)
			{
				
				if (Util::rand() < likelihoodToHitPerTick)
				{
					t->Damage(this, 1);
				}
			}

		}

		float dirX = m_pos.x - o->GetPos().x;
		float dirZ = m_pos.z - o->GetPos().z;

		float dirLen = sqrt(dirX * dirX + dirZ * dirZ);

		if (dirLen > .001f)
		{
			m_frcX += dirX / dirLen * pushOutStrength;
			m_frcZ += dirZ / dirLen * pushOutStrength;
		}
		else
		{
			// literally perfectly inside each other
			o->ForcePos({ m_pos.x + Util::rand()*.02f-.01f, 0, m_pos.z + Util::rand()*.02f-.01f});
		}

	}

}

void Character::DoDamageLogic(Character* pOpposer)
{

	float likelihoodOfHit = likelihoodToHitPerTick;

	if (charType == CHARTYPE_TROOP)
		likelihoodOfHit *= howMuchBetterAreTroops;

	if (Util::rand() < likelihoodOfHit)
	{

		int diff = (int)(Util::rand() * (damageMax - damageMin));
		int damage = damageMin + diff;

		pOpposer->Damage(this, damage);

	}

}

void Character::DoAnim()
{
	bool xAxis = abs(m_velX) > abs(m_velZ);

	float velmgt = sqrt(m_velX * m_velX + m_velZ * m_velZ);

	if (xAxis)
	{
		m_framesYOffs = 2;

		if (m_velX > 0)
			m_framesYOffs++;
	}
	else
	{
		m_framesYOffs = 0;

		if (m_velZ > 0)
			m_framesYOffs++;
	}

	m_framesAnimTime = (m_framesAnimTime + 1) % xAnimPeriod;

	if (velmgt < .1f)
	{
		if (xAxis && m_velX > 0)
		{
			// this one starts on walking :/
			
			if (m_framesAnimTime > xAnimPeriod / numFrames && m_framesAnimTime < xAnimPeriod / numFrames * 2)
			{
				m_framesAnimTime = xAnimPeriod / numFrames + 1;
			}

		}
		else
		{
			if (m_framesAnimTime < xAnimPeriod / numFrames)
			{
				// dont animate when not moving
				m_framesAnimTime = 0;
			}
		}
	}

}

#include "Snd.hpp"

void Character::JustDied(Obj* pKiller)
{

	if (!m_scrnBounds.OnScreen())
	{
		// don't do anything fancy for offscreen deaths
		Remove();
		return;
	}

	Game::Get().GetSnd().Play(Util::rand() < .5f ? SND_DEATH0 : SND_DEATH1);

	World* pWorld = Game::Get().GetWorld();

	if (charType == CHARTYPE_ZOMBIE)
	{
		float minCoins = (float)GOLDCOST_GEN_ZOMBIEKILL_MIN / (float)GOLDCOST_COIN;
		float maxCoins = (float)GOLDCOST_GEN_ZOMBIEKILL_MAX / (float)GOLDCOST_COIN;

		float nCoins = Util::rand() * (maxCoins - minCoins) + minCoins;

		int wholeCoins = (int)nCoins;

		float partialCoin = nCoins - (float)wholeCoins;

		if (partialCoin > .1f)
		{
			if (Util::rand() < partialCoin) wholeCoins++;
		}

		for (int i = 0; i < wholeCoins; ++i)
		{
			pWorld->NewGameObject<Gold>(m_pos);
		}

	}

	float xDiff{}, zDiff{};

	if (pKiller)
	{
		xDiff = m_pos.x - pKiller->GetPos().x;
		zDiff = m_pos.z - pKiller->GetPos().z;
	}
	
	const uint32_t uNumParticles = 200;
	const float power = 2.f;

	for (uint32_t i = 0; i < uNumParticles; ++i)
	{

		float yDir = -1;
		while (yDir < 0)
		{
			// get positive gamma-distrib value for y
			yDir = Util::rand(true);
		}

		float xDir = Util::rand(true);
		float zDir = Util::rand(true);

		float dirlen = sqrt(xDir * xDir + yDir * yDir + zDir * zDir);
		xDir /= dirlen;
		yDir /= dirlen;
		zDir /= dirlen;

		pWorld->NewGameObject<BloodParticle>(
			Pos
			{
				m_pos.x + Util::rand() * .5f - .25f,
				m_pos.y + Util::rand() * 4,
				m_pos.z + Util::rand() * .5f - .25f
			},

			// Velocity
			Pos
			{
				xDir + xDiff*.1f/* * Util::rand() * power*/,
				yDir/* * Util::rand() * power*/,
				zDir + zDiff*.1f/* * Util::rand() * power*/
			}
		);

	}

	Remove();

}

void Character::JustHurt(Obj* pHurter)
{
	m_hurtCooldown = 20;

	if (m_scrnBounds.OnScreen())
	{
		Game::Get().GetSnd().Play(SND_HIT);
	}



}
