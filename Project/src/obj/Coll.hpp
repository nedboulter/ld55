#pragma once

struct Pos
{
	float x, y, z;
};

struct HalfExtents
{
	float halfWidth;
	float halfDepth;
};

struct Collider
{
public:
	
	// Just 2D for now
	float minX, minZ;
	float maxX, maxZ;


	inline bool Intersects(const Collider& other)
	{
		return
			(minX < other.maxX && maxX > other.minX) &&
			(minZ < other.maxZ && maxZ > other.minZ);
	}

	inline static Collider Dummy() { return { FLT_MAX }; }
	inline bool IsDummy() { return minX == FLT_MAX; }

	inline static Collider FromPos(const Pos& p, float halfWidth, float halfDepth)
	{
		return
		{
			p.x - halfWidth,
			p.z - halfDepth,
			p.x + halfWidth,
			p.z + halfDepth,
		};
	}

};

struct ScrnBounds
{
public:

	float x, y;
	float hw, hh;
	float viewZ;

	bool OnScreen() const
	{
		return
			(x - hw < WIDTH && x + hw > 0) &&
			(y - hh < HEIGHT && y + hh > 0);
	}

	void Combine(ScrnBounds other)
	{
		float minX = std::min(x - hw, other.x - other.hw);
		float maxX = std::max(x + hw, other.x + other.hw);
		float minY = std::min(y - hh, other.y - other.hh);
		float maxY = std::max(y + hh, other.y + other.hh);

		x = (minX + maxX) / 2.f;
		y = (minY + maxY) / 2.f;

		hw = maxX - x;
		hh = maxY - y;

		// use which ever one is closest,
		// 'most visible??!'
		viewZ = std::min(viewZ, other.viewZ);

	}
};
