#include "pch.hpp"
#include "Textures.hpp"

Textures::Textures()
{
	
	if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pImgFactory))))
	{
		DBGERR("Can't load images?!");
	}

	Load("res/textures.png",
		m_spriteSheet,
		m_uSpritesWidth,
		m_uSpritesHeight);

	m_pSpriteSheet = m_spriteSheet.data();

	{
		std::vector<uint32_t> perlin;
		Load("res/perlin.png",
			perlin,
			m_uPerlinWidth,
			m_uPerlinHeight);

		m_perlin.resize(m_uPerlinWidth * m_uPerlinHeight);

		for (uint32_t i = 0; i < (uint32_t)m_perlin.size(); ++i)
		{
			// grab red channel
			m_perlin[i] = (perlin[i] >> 16) & 0xff;
		}

		m_pPerlin = (uint8_t*)m_perlin.data();

	}

}

void Textures::Load(
	const std::string& fName,
	std::vector<uint32_t>& out_bits,
	uint32_t& out_width,
	uint32_t& out_height)
{
	std::wstring wFName(fName.begin(), fName.end());

	auto HRes = [](HRESULT r) { if (FAILED(r)) { DBGERR("Failed to load textures!"); } };
	auto Rel = [](auto*& pComObj) { if (pComObj) { pComObj->Release(); pComObj = nullptr; } };

	IWICBitmapDecoder* pDecoder{};
	IWICBitmapFrameDecode* pFrame{};
	IWICFormatConverter* pFrameConverter{};

	HRes(pImgFactory->CreateDecoderFromFilename(wFName.c_str(), nullptr,
		GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder));

	HRes(pDecoder->GetFrame(0, &pFrame));

	HRes(pImgFactory->CreateFormatConverter(&pFrameConverter));

	HRes(pFrameConverter->Initialize(pFrame, GUID_WICPixelFormat32bppBGRA,
		WICBitmapDitherTypeNone, nullptr, 0., WICBitmapPaletteTypeCustom));

	HRes(pFrameConverter->GetSize(&out_width, &out_height));

	out_bits.resize(out_width * out_height);

	HRes(pFrameConverter->CopyPixels(nullptr, out_width * sizeof(uint32_t),
		out_width * out_height * sizeof(uint32_t), (BYTE*)out_bits.data()));

	Rel(pFrameConverter);
	Rel(pFrame);
	Rel(pDecoder);
}

void Textures::Blit(uint32_t* pScrn,
	const TexRegion& src,
	int xOffs,
	int yOffs)
{

	constexpr int tileSheetWidthPix = tileSheetWidth * tileSize;
	constexpr int tileSheetHeightPix = tileSheetHeight * tileSize;

	if (src.x < 0 || src.y < 0 ||
		src.x + src.w > tileSheetWidthPix ||
		src.y + src.h > tileSheetHeightPix)
	{
		DBGERR("Src region oob!");
	}

	int minX = xOffs;
	int minY = yOffs;
	int maxX = minX + src.w;
	int maxY = minY + src.h;

	int minXC = std::clamp(minX, 0, WIDTH);
	int minYC = std::clamp(minY, 0, HEIGHT);
	int maxXC = std::clamp(maxX, 0, WIDTH);
	int maxYC = std::clamp(maxY, 0, HEIGHT);

	for (int y = minYC; y < maxYC; ++y)
	{
		int ySrcOffs = y - minY + src.y;

		for (int x = minXC; x < maxXC; ++x)
		{
			int xSrcOffs = x - minX + src.x;

			uint32_t bgra = m_pSpriteSheet[xSrcOffs + ySrcOffs * tileSheetWidthPix];

			if (((bgra >> 24) & 0xff) == 0xff)
				pScrn[x + y * WIDTH] = bgra;

		}

	}

}

void Textures::StretchBlit(uint32_t* pScrn,
	const TexRegion& src,
	int xOffs,
	int yOffs,
	int width,
	int height)
{

	constexpr int tileSheetWidthPix = tileSheetWidth * tileSize;
	constexpr int tileSheetHeightPix = tileSheetHeight * tileSize;

	if (src.x < 0 || src.y < 0 ||
		src.x + src.w > tileSheetWidthPix ||
		src.y + src.h > tileSheetHeightPix)
	{
		DBGERR("Src region oob!");
	}

	int minX = xOffs;
	int minY = yOffs;
	int maxX = minX + width;
	int maxY = minY + height;

	int minXC = std::clamp(minX, 0, WIDTH);
	int minYC = std::clamp(minY, 0, HEIGHT);
	int maxXC = std::clamp(maxX, 0, WIDTH);
	int maxYC = std::clamp(maxY, 0, HEIGHT);

	for (int y = minYC; y < maxYC; ++y)
	{
		float yLrp = (y - minY) / (float)(maxY - minY);
		int ySrcOffs = (int)(yLrp * src.h) + src.y;

		for (int x = minXC; x < maxXC; ++x)
		{
			float xLrp = (x - minX) / (float)(maxX - minX);
			int xSrcOffs = (int)(xLrp * src.w) + src.x;

			uint32_t bgra = m_pSpriteSheet[xSrcOffs + ySrcOffs * tileSheetWidthPix];

			if ((bgra >> 24 & 0xff) == 0xff)
				pScrn[x + y * WIDTH] = bgra;

		}
	}

}

void Textures::Destroy()
{

	if (pImgFactory)
	{
		pImgFactory->Release();
		pImgFactory = nullptr;
	}
}
