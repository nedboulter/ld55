#include "pch.hpp"
#include "Game.hpp"

#include "Text.hpp"

Text* Text::s_text = nullptr;
uint32_t Text::s_numText = 0;

static void GrabMouse(HWND w)
{
	RECT wr;
	GetWindowRect(w, &wr);

	// Clip to one pixel in the middle
	RECT cr =
	{
		(wr.left + wr.right) / 2,
		(wr.bottom + wr.top) / 2
	};
	cr.right = cr.left + 1;
	cr.bottom = cr.top + 1;

	if (!ClipCursor(&cr)) DBGLOG("Couldn't clip cursor??");

	// Hide
	while (ShowCursor(FALSE) > 0);

}

static void ReleaseMouse()
{
	ClipCursor(NULL);

	// Show
	while (ShowCursor(TRUE) < 0);
}

static float s_sensitivity = .25f;
static uint32_t s_sensChangedCooldown = 0;

static LRESULT CALLBACK WndProc(HWND win, UINT msg, WPARAM w, LPARAM l)
{

	Game& game = Game::Get();

	switch (msg)
	{
	case WM_INPUT:

		// Getting mouse input
			

		if (game.MouseGrabbed())
		{

			RAWINPUT data{};
			uint32_t sz = sizeof(data);
			if (GetRawInputData((HRAWINPUT)l, RID_INPUT, &data, &sz, sizeof(RAWINPUTHEADER)) != sz)
				DBGERR("Erm...");

			RAWMOUSE& mouse = data.data.mouse;

			// drive sens with scroll
			if (mouse.usButtonFlags & RI_MOUSE_WHEEL)
			{
				const float incr = .01f;
				
				SHORT wheelShort = (SHORT)(mouse.usButtonData) / WHEEL_DELTA;
				s_sensitivity += incr * wheelShort;

				if (s_sensitivity < 0)
					s_sensitivity = 0;

				s_sensChangedCooldown = 120;

				DBGLOG("MOUSE SENSITIVITY CHANGED BY SCROLLWHEEL: {}", s_sensitivity);

			}

			// Mmove (if this isn't relative this is fucked)
			if (mouse.lLastX != 0 || mouse.lLastY != 0)
				game.MouseEvt(MOUSEEVTTYPE_MOVE, s_sensitivity * data.data.mouse.lLastX, s_sensitivity * data.data.mouse.lLastY);

		}


		break;

	case WM_CLOSE:
		game.Quit();
		break;
	case WM_LBUTTONDOWN:

		if (!game.MouseGrabbed())
		{
			GrabMouse(win);
			game.SetMouseGrabbed(true);
		}
		else
		{
			game.MouseEvt(MOUSEEVTTYPE_CLICK, 0, 0);
		}

		break;
	case WM_KEYDOWN:
		if (w == VK_ESCAPE && game.MouseGrabbed())
		{
			ReleaseMouse();
			game.SetMouseGrabbed(false);
		}

		break;
	case WM_KILLFOCUS:
		if (game.MouseGrabbed())
		{
			ReleaseMouse();
			game.SetMouseGrabbed(false);
		}

		break;
	default:
		return DefWindowProc(win, msg, w, l);
	}


	return 0;

}

static double GetTime()
{
	static auto last = std::chrono::steady_clock::now();
	auto now = std::chrono::steady_clock::now();
	return std::chrono::duration<double>(now - last).count();
}

static void DrawMyText(HDC dc)
{
	SetBkMode(dc, TRANSPARENT);

	for (uint32_t u = 0; u < Text::s_numText; ++u)
	{
		Text& t = Text::s_text[u];

		RECT rct = { t.m_xPos, t.m_yPos, WIDTH, HEIGHT };
		RECT rctA = { t.m_xPos+1, t.m_yPos+1, WIDTH, HEIGHT };
		
		// Shadow
		SetTextColor(dc, RGB(0, 0, 0));
		DrawTextA(dc, t.m_str.c_str(), -1, &rctA, DT_NOCLIP | DT_LEFT | DT_TOP);

		//Text
		SetTextColor(dc, t.m_col);
		DrawTextA(dc, t.m_str.c_str(), -1, &rct, DT_NOCLIP | DT_LEFT | DT_TOP);

	}

	// Clear for enxt frame
	Text::Clear();
}

// Monolyth main (just trying to shove all boring stuff here in a mess)
#if 1
int main()
{
	HINSTANCE inst = GetModuleHandle(NULL);
	int nCmdShow = SW_SHOW;
#else
int WINAPI WinMain(HINSTANCE inst, HINSTANCE pInst, LPSTR fname, int nCmdShow)
{
#endif

	SetProcessDPIAware();

	timeBeginPeriod(1);

	if (FAILED(CoInitialize(NULL)))
		DBGERR("CoInitialize fail!");

	Text::Init();

	WNDCLASS wc = {};
	wc.hInstance = inst;
	wc.lpszClassName = "wc";
	wc.lpfnWndProc = WndProc;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClass(&wc))
		DBGERR("Class reg fail!");

	// Register mouse
	RAWINPUTDEVICE dev =
	{
		.usUsagePage = 0x01,
		.usUsage = 0x02,
		.dwFlags = 0,
		.hwndTarget = NULL
	};
	if (!RegisterRawInputDevices(&dev, 1, sizeof(RAWINPUTDEVICE)))
		DBGERR("Failed to register mouse!");

	HWND win = CreateWindow("wc", TITLE, WS_OVERLAPPEDWINDOW,
		0, 0, WIDTH*SCALE, HEIGHT*SCALE,
		NULL, NULL, inst, NULL);
	
	HDC winDC = GetDC(win);
	if (!winDC)
		DBGERR("Bleg");

	uint32_t* pPixels = new uint32_t[WIDTH * HEIGHT];

	HBITMAP scrn = CreateCompatibleBitmap(winDC, WIDTH, HEIGHT);
	HDC scrnDC = CreateCompatibleDC(winDC);
	SelectObject(scrnDC, scrn);

	RECT full, client;
	GetWindowRect(win, &full);
	GetClientRect(win, &client);

	UINT w = (full.right - full.left) * 2 - client.right;
	UINT h = (full.bottom - full.top) * 2 - client.bottom;

	MoveWindow(win,
		GetSystemMetrics(SM_CXSCREEN) / 2 - w / 2,
		GetSystemMetrics(SM_CYSCREEN) / 2 - h / 2,
		w, h, FALSE);

	if (!win)
		DBGERR("Win create fail!");

	Game& game = Game::Get();
	game.Init();

	UpdateWindow(win);
	ShowWindow(win, nCmdShow);

	int ticks = 0, frames = 0;
	double lastTime = GetTime();
	double secTime = lastTime;
	double unprocTicks = 0;

	HBRUSH bkgBrush = CreateSolidBrush(RGB(10, 10, 10));
	RECT oldWS = {};

	while (game.IsRunning())
	{
		double now = GetTime();
		unprocTicks += (now - lastTime) * 60.;
		lastTime = now;

		if (now - secTime >= 1.)
		{
			DBGLOG("{} ticks, {} frames!", ticks, frames);
			ticks = 0;
			frames = 0;
			secTime = now;
		}

		if (unprocTicks >= 1.)
		{
			game.Tick();
			if (s_sensChangedCooldown > 0) --s_sensChangedCooldown;
			++ticks;
			unprocTicks -= 1.;
		}

		// Game to pixels
		if (s_sensChangedCooldown > 0)
		{
			Text::Write(std::format("Mouse Sensitivity: {}", s_sensitivity), WIDTH / 2-100, HEIGHT / 2, 0xff00ff);
		}

		if (!game.MouseGrabbed())
		{
			static uint32_t u = 0;
			++u;

			if ((u % 120) < 60)
			{
				Text::Write(std::format("Click to focus!", s_sensitivity), WIDTH / 2 - 50, HEIGHT / 2-10, 0xff0000);
			}

		}

		game.Render(pPixels);

		// Pixels to bitmap
		BITMAPINFO inf =
		{
			.bmiHeader =
			{
				.biSize = sizeof(BITMAPINFOHEADER),
				.biWidth = WIDTH,
				.biHeight = -HEIGHT,
				.biPlanes = 1,
				.biBitCount = 32,
				.biCompression = BI_RGB
			}
		};

		if (SetDIBitsToDevice(scrnDC, 0, 0, WIDTH, HEIGHT,
			0, 0, 0, HEIGHT, pPixels, &inf, DIB_PAL_COLORS) == 0)
			DBGERR("Pretty sure the game just broke :/");

		// Text to bitmap (lol last 4 hours of the jam)
		DrawMyText(scrnDC);

		// Bitmap to screen

		RECT ws;
		GetClientRect(win, &ws);

		const bool resize = oldWS.right != ws.right || oldWS.bottom != ws.bottom;

		oldWS = ws;

		if (resize)
		{
			// Can't just do this ev frame,
			// no double buffering so would flicker! :D
			FillRect(winDC, &ws, bkgBrush);
		}

		int h = ws.bottom;
		int w = (int)((WIDTH / (float)HEIGHT) * h);

		if (!StretchBlt(winDC, ws.right / 2 - w / 2, 0, w, h, scrnDC, 0, 0, WIDTH, HEIGHT, SRCCOPY))
			DBGERR("Very broke");

		++frames;

		std::this_thread::sleep_for(std::chrono::milliseconds(2));

		MSG msg{};
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

	}

	DeleteObject(bkgBrush);

	game.Destroy();

	SelectObject(scrnDC, NULL);
	DeleteDC(scrnDC);
	DeleteObject(scrn);

	delete[] pPixels;

	ReleaseDC(win, winDC);
	DestroyWindow(win);

	Text::Destroy();

	CoUninitialize();

	timeEndPeriod(1);

	_CrtDumpMemoryLeaks();

	return 0;
}

