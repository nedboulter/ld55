#pragma once

class Text
{
public:

	static const uint32_t uMAX_TEXT = 256;
	static Text* s_text;
	static uint32_t s_numText;

	int m_xPos;
	int m_yPos;
	std::string m_str;
	uint32_t m_col;

	static void Clear() { s_numText = 0; }

	static void Init()
	{
		s_text = new Text[uMAX_TEXT];
	}

	static void Write(const std::string& str, int x, int y,
		uint32_t col = 0xffffff)
	{
		Text& t = s_text[s_numText++];
		t.m_xPos = x;
		t.m_yPos = y;
		t.m_str = str;
		t.m_col =
			RGB(
				(col >> 16) & 0xff,
				(col >> 8) & 0xff,
				col & 0xff);
	}

	static void Destroy()
	{
		delete[] s_text;
	}

};
