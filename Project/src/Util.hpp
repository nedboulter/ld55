#pragma once

namespace Util
{

	static void MoveToward(
		float& val,
		float goal,
		float incr)
	{

		if (val < goal - incr)
		{
			// Can safely add without surpassing the goal
			val += incr;
		}
		else if (val > goal + incr)
		{
			// can safely subtract
			val -= incr;
		}
		else
		{
			val = goal;
		}

	}

	static float rand(bool gaussian = false)
	{
		static std::mt19937 eng = std::mt19937(std::random_device()());

		return
			gaussian ?
			  std::normal_distribution<float>()(eng)
			: std::uniform_real_distribution<float>(0, .9999f)(eng);
	}
	

};
