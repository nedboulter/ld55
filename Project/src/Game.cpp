#include "pch.hpp"
#include "Game.hpp"

#include "Util.hpp"
#include "Textures.hpp"
#include "obj/World.hpp"
#include "obj/Tree.hpp"
#include "Text.hpp"
#include "Snd.hpp"

void Game::Init()
{
	m_pTextures = new Textures();
	m_pSnd = new Snd();

	SwitchState(GAMESTATE_MENU);
}

void Game::Tick()
{
	Game& g = Game::Get();
	
	// Music toggle
	static bool mWasDown = false;
	bool mDown = g.KeyDown('M');
	bool mPressed = !mWasDown && mDown;
	mWasDown = mDown;

	if (mPressed)
	{
		Snd& s = g.GetSnd();
		if (s.MusicOn()) s.StopMusic();
		else s.PlayMusic();
	}

	if (m_state == GAMESTATE_MENU)
	{
		if (g.KeyDown('P'))
			SwitchState(GAMESTATE_PLAY);
	}

	if (m_state == GAMESTATE_PLAY)
	{

		m_pWorld->Tick();
	}


}

void Game::Render(uint32_t* pScrn)
{

	if (m_state == GAMESTATE_MENU)
	{
		m_pTextures->StretchBlit(pScrn,
			{ 160, 128, 160, 120 },
			0, 0, WIDTH, HEIGHT);
	}


	if (m_state == GAMESTATE_PLAY)
	{
		m_pWorld->Render(pScrn);


	}

	// Draw cursor
	if (m_cursor.active)
	{
		m_pTextures->Blit(pScrn,
			{ 0, 0, 10, 12 },
			(int)m_cursor.x - 9,
			(int)m_cursor.y);
	}

}

void Game::MouseEvt(MouseEvtType t, float dx, float dy)
{
	// Drive cursor
	if (m_cursor.active)
	{

		if (t == MOUSEEVTTYPE_MOVE)
		{
			if (m_cursor.x < WIDTH - dx && m_cursor.x > -dx)
				m_cursor.x += dx;

			if (m_cursor.y < HEIGHT - dy && m_cursor.y > -dy)
				m_cursor.y += dy;
		}


	}


}

void Game::SwitchState(GameState newState)
{
	const GameState oldState = m_state;
	m_state = newState;

	if (oldState == GAMESTATE_PLAY)
	{
		delete m_pWorld;
		m_pWorld = nullptr;
	}

	if (newState == GAMESTATE_PLAY)
	{
		if (m_pWorld) DBGSTALL("This is bad!");

		m_pWorld = new World();
	}

}

void Game::Destroy()
{


	// Should cleanup whatever state we're in
	SwitchState(GAMESTATE_NULL);

	delete m_pSnd;
	delete m_pTextures;
}
