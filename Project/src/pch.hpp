#pragma once

#pragma comment(lib, "winmm.lib")

#define _USE_MATH_DEFINES
#define NOMINMAX

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <fstream>
#include <algorithm>
#include <format>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <random>

#include <Windows.h>
#include <wincodec.h>
#include <xaudio2.h>

// Game consts
#define TITLE "Protect the Realm: LD55 made by Ned Boulter"
#define WIDTH 320
#define HEIGHT 240
#define SCALE 2

inline void __log(const std::string& str)
{
	std::cout << str << "\n";
	OutputDebugString((str + "\n").c_str());
}

inline void __err(const std::string& str)
{
	__log(str);
	MessageBox(NULL, str.c_str(), "FATAL ERROR", MB_OK | MB_ICONERROR);
	abort();
}

inline void __stall(const std::string& str)
{
	__log(str);

	#ifdef _DEBUG
	MessageBox(NULL, str.c_str(), "STALL", MB_OK | MB_ICONWARNING);
	__debugbreak();
	#endif
}

#define DBGLOG(fmt, ...)\
{\
	__log(std::format(fmt, __VA_ARGS__));\
}

#define DBGERR(fmt, ...)\
{\
	__err(std::format(fmt, __VA_ARGS__));\
}

#define DBGSTALL(fmt, ...)\
{\
	__stall(std::format(fmt, __VA_ARGS__));\
}

#define CLASS_NO_COPY(Class)\
	Class(const Class&) = delete;\
	Class& operator=(const Class&) = delete;\
	Class(Class&&) = delete;\
	Class& operator=(Class&) = delete\

template<int Dim>
inline void multMat(float* matA, float* matB, float* result)
{
	for (int y = 0; y < Dim; ++y)
	{
		for (int x = 0; x < Dim; ++x)
		{
			float t = 0;

			for (int i = 0; i < Dim; ++i)
			{
				t += matA[i + y * Dim] * matB[x + i * Dim];
			}

			result[x + y * Dim] = t;
		}
	}
}

template<int Dim>
inline void transformVector(float* mat, float* vec, float* result)
{

	for (int y = 0; y < Dim; ++y)
	{

		float t = 0;
		for (int i = 0; i < Dim; ++i)
		{

			t += mat[i + y * Dim] * vec[i];

		}

		result[y] = t;

	}

}
