#include "pch.hpp"
#include "Snd.hpp"

static constexpr const char* const filenames[] =
{
	"res/sounds/death0.wav",
	"res/sounds/death1.wav",
	"res/sounds/towerdeath.wav",
	"res/sounds/createTower.wav",
	"res/sounds/footstep.wav",
	"res/sounds/hitHurt.wav",
	"res/sounds/lowhealth.wav",
	"res/sounds/pickupCoin.wav"
};
static_assert(_countof(filenames) == SND_COUNT, "Snd file mismatch");

Snd::Snd()
{

	HRes(XAudio2Create(&m_pAudio, 0, XAUDIO2_DEFAULT_PROCESSOR));
	if (!m_usingAudio) return;

	HRes(m_pAudio->CreateMasteringVoice(&m_pMaster));
	if (!m_usingAudio) return;

	LoadAudioFiles();
	if (!m_usingAudio)return;

	CreateVoices();
	if (!m_usingAudio) return;

	StartMusic();
	if (!m_usingAudio) return;

#ifdef _DEBUG
	// so annoying
	StopMusic();
#endif

}

void Snd::Play(Snds snd, float volume)
{
	if (!m_usingAudio) return;

	IXAudio2SourceVoice* m_pVoice = m_ppVoices[uNextVoice];
	uNextVoice = (uNextVoice + 1) % m_uVoiceCount;

	HRes(m_pVoice->SetVolume(volume));
	HRes(m_pVoice->Stop(0));
	HRes(m_pVoice->FlushSourceBuffers());
	HRes(m_pVoice->SubmitSourceBuffer(&m_audioBuffers[snd]));
	HRes(m_pVoice->Start(0));


}

static bool LoadAudio(
	const char* wavFile,
	WAVEFORMATEX& resFmt,
	XAUDIO2_BUFFER& resBuffer,
	bool loop = false)
{
	std::ifstream inStream(wavFile, std::ios::binary);
	if (!inStream.good())
	{
		DBGLOG("File \"{}\" not found!", wavFile);
		return false;
	}

	// Wave format is 20 bytes in
	inStream.seekg(20, std::ios::beg);
	inStream.read((char*)&resFmt, sizeof(WAVEFORMAT));
	inStream.read((char*)&resFmt.wBitsPerSample, sizeof(resFmt.wBitsPerSample));

	resBuffer.Flags = XAUDIO2_END_OF_STREAM;
	
	inStream.seekg(40, std::ios::beg);
	inStream.read((char*)&resBuffer.AudioBytes, sizeof(uint32_t));

	resBuffer.pAudioData = new BYTE[resBuffer.AudioBytes];
	inStream.read((char*)resBuffer.pAudioData, resBuffer.AudioBytes);

	if (loop)
	{

		resBuffer.LoopBegin = 0;
		resBuffer.LoopLength = 0;// loop entire thing
		resBuffer.LoopCount = XAUDIO2_LOOP_INFINITE;

	}

	return true;

}

void Snd::LoadAudioFiles()
{

	if (!LoadAudio("res/sounds/music.wav", m_musicFmt, m_musicBuffer, true))
	{
		m_usingAudio = false;
		return;
	}

	for (uint32_t u = 0; u < SND_COUNT; ++u)
	{
		const char* fname = filenames[u];
		
		WAVEFORMATEX wvFmt{};
		if (!LoadAudio(fname, wvFmt, m_audioBuffers[u]))
		{
			m_usingAudio = false;
			return;
		}

		if (m_clipFmt.nAvgBytesPerSec == 0)
		{
			memcpy(&m_clipFmt, &wvFmt, sizeof(wvFmt));
		}
		else
		{
			char* a = (char*)&m_clipFmt, * b = (char*)&wvFmt;
			for (int i = 0; i < sizeof(WAVEFORMAT); ++i)
			{
				if (a[i] != b[i])
				{
					DBGLOG("Clip formats mismatch!!");
					m_usingAudio = false;
					return;
				}
			}
		}
	}

}

void Snd::CreateVoices()
{

	// Music
	HRes(m_pAudio->CreateSourceVoice(&m_pMusicVoice, &m_musicFmt));
	if (!m_usingAudio)return;

	// Pool
	m_ppVoices = new IXAudio2SourceVoice*[m_uVoiceCount];

	for (uint32_t u = 0; u < m_uVoiceCount; ++u)
	{
		HRes(m_pAudio->CreateSourceVoice(&m_ppVoices[u], &m_clipFmt));
		if (!m_usingAudio)return;
	}

}

void Snd::StartMusic()
{
	if (!m_usingAudio)
		return;

	HRes(m_pMusicVoice->SubmitSourceBuffer(&m_musicBuffer));
	HRes(m_pMusicVoice->Start());

}

Snd::~Snd()
{

	if (m_ppVoices)
	{
		delete[] m_ppVoices;
		m_ppVoices = nullptr;
	}

	if (m_musicBuffer.pAudioData)
	{
		delete[] m_musicBuffer.pAudioData;
		m_musicBuffer.pAudioData = nullptr;
	}

	for (uint32_t u = 0; u < SND_COUNT; ++u)
	{
		if (m_audioBuffers[u].pAudioData)
		{
			delete[] m_audioBuffers[u].pAudioData;
			m_audioBuffers[u].pAudioData = nullptr;
		}
	}

	if (m_ppVoices)
	{
		delete[] m_ppVoices;
		m_ppVoices = nullptr;
	}

	if (m_pAudio)
	{
		m_pAudio->Release();
		m_pAudio = nullptr;
	}
}
