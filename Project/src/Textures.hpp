#pragma once

struct TexRegion
{
	int x, y, w, h;
};

class Textures
{
public:

	Textures();

	CLASS_NO_COPY(Textures);

	void Load(
		const std::string& fName,
		std::vector<uint32_t>& out_bits,
		uint32_t& out_width,
		uint32_t& out_height);

	void Blit(uint32_t* pScrn,
		const TexRegion& src,
		int xOffs,
		int yOffs);

	void StretchBlit(uint32_t* pScrn,
		const TexRegion& src,
		int xOffs,
		int yOffs,
		int width,
		int height);

	// 0-255, maps to 0, 1
	uint8_t Noise(float x, float y)
	{
		float xx = (x - floor(x)) * .99f;
		float yy = (y - floor(y)) * .99f;

		int xpos = (int)(xx * m_uPerlinWidth);
		int ypos = (int)(yy * m_uPerlinHeight);

		return m_pPerlin[xpos + ypos * m_uPerlinWidth];
	}

	inline ~Textures() { Destroy(); }

private:

	static constexpr int tileSize = 16;
	static constexpr int tileSheetWidth = 20;// tiles
	static constexpr int tileSheetHeight = 20;// tiles


	IWICImagingFactory* pImgFactory{};
	
	std::vector<uint32_t> m_spriteSheet;
	uint32_t* m_pSpriteSheet{};
	uint32_t m_uSpritesWidth{}, m_uSpritesHeight{};

	std::vector<char> m_perlin;
	uint8_t* m_pPerlin{};
	uint32_t m_uPerlinWidth{}, m_uPerlinHeight{};

	void Destroy();

};
