#pragma once

enum Snds
{

	SND_DEATH0,
	SND_DEATH1,
	SND_DEATHTOWER,
	SND_CREATETOWER,
	SND_FOOTSTEP,
	SND_HIT,
	SND_LOWHEALTH,
	SND_COIN,

	SND_COUNT,
};

class Snd
{
public:

	Snd();
	CLASS_NO_COPY(Snd);

	void Play(Snds snd, float volume = .25f);

	inline void PlayMusic()
	{
		m_pMusicVoice->Start(0);
		m_musicOn = true;
	}

	inline bool MusicOn() { return m_musicOn; }

	void StopMusic()
	{
		m_pMusicVoice->Stop(0);
		m_musicOn = false;
	}

	~Snd();

private:

	bool m_usingAudio = true;
	bool m_musicOn = true;

	IXAudio2* m_pAudio{};
	IXAudio2MasteringVoice* m_pMaster{};

	XAUDIO2_BUFFER m_musicBuffer{};
	XAUDIO2_BUFFER m_audioBuffers[SND_COUNT]{};

	WAVEFORMATEX m_musicFmt{};
	IXAudio2SourceVoice* m_pMusicVoice{};

	WAVEFORMATEX m_clipFmt{};
	const uint32_t m_uVoiceCount = 50;
	IXAudio2SourceVoice** m_ppVoices{};
	uint32_t uNextVoice = 0;

	inline void HRes(HRESULT r)
	{
		if (FAILED(r))
		{
			DBGLOG("Audio Error!");
			m_usingAudio = false;
		}
	}

	void LoadAudioFiles();

	void CreateVoices();

	void StartMusic();

};
