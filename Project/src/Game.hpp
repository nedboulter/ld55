#pragma once

enum MouseEvtType
{
	MOUSEEVTTYPE_CLICK,
	MOUSEEVTTYPE_MOVE
};

enum GameState
{
	GAMESTATE_NULL,
	GAMESTATE_MENU,
	GAMESTATE_PLAY,
};

class Textures;
class World;
class Snd;

struct Cursor
{
	bool active = true;
	float x = WIDTH/2.f, y=HEIGHT/2.f;
};

class Game
{
public:

	CLASS_NO_COPY(Game);

	void Init();

	void Tick();

	void Render(uint32_t* pScrn);

	void MouseEvt(MouseEvtType t, float dx, float dy);

	inline bool MouseGrabbed() const
	{
		return m_mouseGrabbed;
	}

	inline void SetMouseGrabbed(bool b) { m_mouseGrabbed = b; }

	inline bool KeyDown(int kc)
	{
		// Focus can be whether the mouse is grabbed lol...
		const bool focus = m_mouseGrabbed;

		return focus && GetKeyState(kc) < 0;
	}

	inline Cursor& GetCursor() { return m_cursor; }
	inline const Cursor& GetCursor() const { return m_cursor; }

	inline bool MouseDown() { return KeyDown(VK_LBUTTON); }
	inline bool RightMouseDown() { return KeyDown(VK_RBUTTON); }

	inline Textures& GetTextures() { return *m_pTextures; }
	inline World* GetWorld() { return m_pWorld; } // will be null if not play
	inline GameState GetState() { return m_state; }
	void SwitchState(GameState newState);

	void Destroy();

	inline bool IsRunning() const { return m_running; }

	inline void Quit() { m_running = false; }

	inline static Game& Get()
	{
		static Game game;
		return game;
	}

	inline Snd& GetSnd() { return *m_pSnd; }

private:

	Game() {}

	bool m_running = true;
	bool m_mouseGrabbed = false;

	GameState m_state = GAMESTATE_NULL;

	Textures* m_pTextures{};
	Cursor m_cursor{};

	World* m_pWorld{};

	Snd* m_pSnd{};

};
